$('input#patient').typeahead({
  source: ['#167-786 - Homme - 38 ans', '#987-154 - Femme - 22 ans', '#234-165 - Homme - 12 ans']
});

$("input.image_select").bind("click", function(e) {
	var selected_block = $(this).parent();
	var row = selected_block.parent();
	$(".col-md-1", row).hide();
	selected_block.show();
	$("#image_selector").show();
});




$("#image_modal img").bind("click", function(e) {
	e.preventDefault();

	$('#image_modal').modal('hide');
});

$('#image_modal').on('hidden.bs.modal', function () {
	var to_show = $('#image_modal').data("target_show");
	var to_hide = $('#image_modal').data("target_hide");

  $('#'+to_show).show();
  $("#"+to_hide).hide();
});

$("input[type='checkbox']").bootstrapSwitch({
	onText: "Oui",
	offText: "Non",
	size: "mini"
});

$("#embolie_pulmonaire").on('switchChange.bootstrapSwitch', function(event, state) {
	if (state) {
		$("#image_embolie").show();
	} else {
		$("#image_embolie").hide();
	}
});

$("#image_modal_1").bind("click", function(e) {
	e.preventDefault();

	$('#tools_sketch').sketch({defaultColor: "#FF2A2A"});

	$('#image_modal').data("target_show", "tools");
	$('#image_modal').data("target_hide", "image_selector");
	$('#image_modal').modal('show');
});

$("#image_modal_2").bind("click", function(e) {
	e.preventDefault();

	$('#embolie_tools_sketch').sketch({defaultColor: "#FF2A2A"});

	$('#image_modal').data("target_show", "image_embolie_finale");
	$('#image_modal').data("target_hide", "image_embolie_picker");
	$('#image_modal').modal('show');
});


$("textarea#antecedents").focus(function(e) {
	$(this).html("H 38ans. Sans ATCD. AVP Voiture incarcéré MI - Hémodynamique stable.");
});

$("textarea#conclusion").focus(function(e) {
	$(this).html("Fracture diaphysaire en coin type B3 selon classification de l'A0. Embolie pulmonaire droite proximale a debut de retentissement cardiaque droit");
});

$("input#ratio").focus(function(e) {
	$(this).val("0.9");
});